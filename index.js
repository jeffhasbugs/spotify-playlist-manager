// ======HELPER FUNCTIONS=====
function randomUUID(len) {
    let arr = new Uint8Array((len || 20) / 2);
    window.crypto.getRandomValues(arr);
    return Array.from(arr, (val)=>{
        return val.toString(16).padStart(2, "0");
    }).join('');
}
// ======HELPER FUNCTIONS=====

function initializeApp() {
    let client_id = 'b587bc014e6745ea8f376e6debe96b77';
    let redirect_uri = 'http://localhost:3000/callback.html';
    
    //let state = generateRandomString(16);
    let state = randomUUID(16);
    
    localStorage.setItem("state", state);
    let scope = 'user-read-email playlist-modify-public playlist-modify-private ugc-image-upload';
    
    let url = 'https://accounts.spotify.com/authorize';
    url += '?response_type=token';
    url += '&client_id=' + encodeURIComponent(client_id);
    url += '&scope=' + encodeURIComponent(scope);
    url += '&redirect_uri=' + encodeURIComponent(redirect_uri);
    url += '&state=' + encodeURIComponent(state);
    
    window.location.href = url;
}

function clearApp() {
    localStorage.clear();
    window.location.reload();
}

function usernameBtnClick() {
    if (access_token) {
        usernameBtn.innerHTML = "Sign in";
        clearApp();
    }
    else {
        initializeApp();
    }
}

function makeHttpRequest(method, url, headers, data, onload) {
    if (method === "POST" && ! data)
        throw "No data parameter provided";
    let request = new XMLHttpRequest();

    request.open(method, url);
    for (let item in headers) {
        request.setRequestHeader(item, headers[item]);
    }
    request.send(data);

    request.onload = onload;
}

function fetchPlaylistTracks(playlist_id) {
    const url = `https://api.spotify.com/v1/playlists/${playlist_id}/tracks`;
    let access_token = "Bearer " + localStorage.getItem("access_token");

    let metadata_url = url + "?offset=0&limit=1";
    makeHttpRequest("GET", metadata_url, {"authorization":  access_token, 
        "content-type": "application/json"}, null, ()=>{
            if (this.readyState === XMLHttpRequest.DONE && this.status === 200)
                localStorage.setItem("playlistSize", 186);
        });

    const total = localStorage.getItem("playlistSize");
    const limit = 100;
    let offset = 0;
    let arr = new Array();
    
    while (offset < total) {
        makeHttpRequest("GET", url, {"authorization":  access_token, 
            "content-type": "application/json"}, null, () => {
                if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    let items = this.response.items;
                    for (item in items) {
                        arr.push(items[item].track.uri);
                    }
                }
        });
        offset += limit;
    }
    localStorage.setItem("playlistBuffer", arr);
}

function uploadPlaylist(playlist_id) {
    let url = `https://api.spotify.com/v1/playlists/${playlist_id}/tracks`;
    let access_token = "Bearer " + localStorage.getItem("access_token");

    // max upload 100 tracks
    let arr = localStorage.getItem("playlistBuffer");
    let total = localStorage.getItem("playlistSize");
    let index = 0;
    while (total >= 0) {
        let data = null;
        if (total >= 100) {
            total -= 100; index += 100;
            data = arr.slice(index, index+100);
        }
        else {
            data = arr.slice(index);
        }
        makeHttpRequest("POST", url, {"authorization":  access_token,
            "content-type": "application.json"}, {"uris":data}, null);
    }
}

let access_token = localStorage.getItem("access_token");
let usernameBtn = document.getElementById('usernameBtn');
    
usernameBtn.addEventListener("click", usernameBtnClick);
if (access_token)
    usernameBtn.innerHTML = "Sign out";
else
    usernameBtn.innerHTML = "Sign in";

let playlistFetchBtn = document.getElementById('playlistFetchBtn');
playlistFetchBtn.addEventListener("click", ()=>{
    let input_id = document.getElementById('inputPlaylist').value;
    return fetchPlaylistTracks(input_id);
});


